/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.johnricaurte.clasesjava.fxexample.entidades;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public enum Genero {
    MASCULINO,
    FEMENINO;
}
