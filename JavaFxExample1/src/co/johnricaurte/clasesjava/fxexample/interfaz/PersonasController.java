/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.johnricaurte.clasesjava.fxexample.interfaz;

import co.johnricaurte.clasesjava.fxexample.entidades.Genero;
import co.johnricaurte.clasesjava.fxexample.entidades.Persona;
import co.johnricaurte.clasesjava.fxexample.servicios.PersonaService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class PersonasController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML injects">
    @FXML
    private TableView<Persona> resultadosTbl;

    @FXML
    private TextField nombreTxf;

    @FXML
    private Label label;

    @FXML
    private Spinner<Integer> edadSpn;

    @FXML
    private ChoiceBox<Genero> generoChb;

    @FXML
    private Button previousBtn;

    @FXML
    private Button nuevoBtn;

    @FXML
    private Button guardarBtn;

    @FXML
    private Button eliminarBtn;

    @FXML
    private Button nextBtn;
    //</editor-fold>

    private PersonaService personaService;
    private Integer currentIndex;

    @FXML
    void eliminarBtnAction(ActionEvent event) {

    }

    @FXML
    void guardarBtnAction(ActionEvent event) {

    }

    @FXML
    void nextBtnAction(ActionEvent event) {
        if (currentIndex == null || currentIndex == 0) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }
        resultadosTbl.getSelectionModel().select(currentIndex);
    }

    @FXML
    void nuevoBtnAction(ActionEvent event) {
        currentIndex = null;
        limpiarFormulario();
    }

    @FXML
    void previousBtnAction(ActionEvent event) {
        int lastIndex = personaService.getPersonas().size() - 1;
        if (currentIndex == null || currentIndex == lastIndex) {
            currentIndex = lastIndex;
        } else {
            currentIndex--;
        }
        resultadosTbl.getSelectionModel().select(currentIndex);
    }

    private void limpiarFormulario() {
        nombreTxf.clear();
        edadSpn.getValueFactory().setValue(0);
        generoChb.getSelectionModel().select(0);
        resultadosTbl.getSelectionModel().clearSelection();
    }

    private void cargarPersonaEnFormulario(Persona persona) {
        if (persona != null) {
            nombreTxf.setText(persona.getNombre());
            edadSpn.getValueFactory().setValue(persona.getEdad());
            generoChb.getSelectionModel().select(persona.getGenero());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        personaService = new PersonaService();

        generoChb.getItems().addAll(Genero.values());
        generoChb.getSelectionModel().select(0);

        recargarTabla();
        ChangeListener<Persona> changeListener = new ChangeListener<Persona>() {
            @Override
            public void changed(ObservableValue<? extends Persona> observable,
                    Persona oldValue, Persona newValue) {
                if (newValue != null) {
                    cargarPersonaEnFormulario(newValue);
                }
            }
        };
        resultadosTbl.getSelectionModel().selectedItemProperty().addListener(changeListener);
        resultadosTbl.getColumns().get(0).setCellValueFactory(new PropertyValueFactory("nombre"));
        resultadosTbl.getColumns().get(1).setCellValueFactory(new PropertyValueFactory("edad"));
        resultadosTbl.getColumns().get(2).setCellValueFactory(new PropertyValueFactory("genero"));

        edadSpn.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0));

        limpiarFormulario();
    }

    private void recargarTabla() {
        resultadosTbl.getItems().clear();
        resultadosTbl.getItems().addAll(personaService.getPersonas());
    }
}
