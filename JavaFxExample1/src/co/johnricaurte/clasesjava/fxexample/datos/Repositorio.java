/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.johnricaurte.clasesjava.fxexample.datos;

import co.johnricaurte.clasesjava.fxexample.entidades.Genero;
import co.johnricaurte.clasesjava.fxexample.entidades.Persona;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Repositorio {
    
    private final List<Persona> personas;
    
    private Repositorio() {
        personas = Arrays.asList(
                new Persona("Fulana de tal", 20, Genero.FEMENINO),
                new Persona("Mengano", 17, Genero.FEMENINO),
                new Persona("Perengano", 32, Genero.FEMENINO)
        );
    }
    
    
    public static Repositorio getInstance() {
        return RepositorioHolder.INSTANCE;
    }
    
    private static class RepositorioHolder {

        private static final Repositorio INSTANCE = new Repositorio();
    }

    /**
     * @return the personas
     */
    public List<Persona> getPersonas() {
        return new ArrayList(personas);
    }
}
