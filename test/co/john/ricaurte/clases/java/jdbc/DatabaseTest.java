/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc;

import co.john.ricaurte.clases.java.jdbc.sample.DatabaseInitialDataIntegrationTest;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DatabaseConfiguration;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DataBaseConnection;
import co.john.ricaurte.clases.java.jdbc.sample.daos.HeroDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class DatabaseTest {

    static List<Person> people = new ArrayList<>();

    public DatabaseTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        createTestTable();
        people.add(new Person(20, "FEMALE", "Foo"));
        people.add(new Person(17, "MALE", "Bar"));
        people.add(new Person(18, "MALE", "Baz"));
        people.add(new Person(30, "FEMALE", "Qux"));
    }

    @Test
    public void testQUeryAdultPeople() {
        createInitialData();
        List<Person> adultPeople = findAdultPeople();
        for(Person person: adultPeople){
            Assert.assertTrue(person.age >= 18);
        }
    }

    private List<Person> findAdultPeople() {
        try (Connection connection = DataBaseConnection.getConnection();
                PreparedStatement prepareStatement = connection
                        .prepareStatement("SELECT p.* FROM Person p WHERE p.age >= ? ");) {

            prepareStatement.setInt(1, 18);
            ResultSet resultSet = prepareStatement.executeQuery();
            List<Person> foundPersons = new ArrayList<>();
            while (resultSet.next()) {
                foundPersons.add(new Person(
                        resultSet.getInt("id"),
                        resultSet.getInt("age"),
                        resultSet.getString("gender"),
                        resultSet.getString("name")));
            }
            return foundPersons;
        } catch (SQLException ex) {
            Logger.getLogger(HeroDao.class.getName()).log(Level.SEVERE, null, ex);
            return Collections.emptyList();
        }
    }

    private static void createTestTable() {

        try (Connection connection = DataBaseConnection.getConnection();
                Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE Person (   "
                    + "    id       INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                    + "    age      INTEGER NOT NULL, "
                    + "    gender   VARCHAR(6), "
                    + "    name     VARCHAR(100), "
                    + "    PRIMARY KEY (id) "
                    + ")");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void createInitialData() {
        String InsertSql = "INSERT INTO Person (age, gender, name) "
                + "VALUES (?, ?, ?)";
        try (Connection connection = DataBaseConnection.getConnection();
                PreparedStatement prepareStatement = connection
                        .prepareStatement(InsertSql, Statement.RETURN_GENERATED_KEYS);) {

            for (Person person : people) {
                // Set values to wilcards ?
                prepareStatement.setInt(1, person.age);
                prepareStatement.setString(2, person.gender);
                prepareStatement.setString(3, person.name);

                // excecute the query
                prepareStatement.executeUpdate();

                // get generated keys
                try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        person.id = generatedKeys.getInt(1);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseInitialDataIntegrationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class Person {

    Integer id;
    Integer age;
    String gender;
    String name;

    public Person(Integer id, Integer age, String gender, String name) {
        this.id = id;
        this.age = age;
        this.gender = gender;
        this.name = name;
    }

    public Person(Integer age, String gender, String name) {
        this.age = age;
        this.gender = gender;
        this.name = name;
    }

}
