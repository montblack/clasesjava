/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample;

import co.john.ricaurte.clases.java.jdbc.sample.utils.QueryUtil;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DatabaseConfiguration;
import java.sql.SQLException;
import java.util.List;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Test;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class DatabaseInitialDataIntegrationTest {

    public DatabaseInitialDataIntegrationTest() {
    }

    /**
     * Test that create entities method should create the expected tree tables.
     *
     * @throws SQLException connection or query exception
     */
    @Test
    public void createEntities_ShouldCreateExpectedEntities() throws SQLException {

        DatabaseConfiguration.configureDatabase();

        List<String> tableNames = QueryUtil.query(
                "SELECT TABLENAME FROM sys.systables WHERE TABLETYPE = 'T'", 
                new StringJdbcMapperImpl());

        MatcherAssert.assertThat(tableNames, 
                IsCollectionContaining.hasItems("POWER", "HERO", "HEROPOWER"));
    }
}
