/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.generics;

/**
 * Example for simple generic implementation.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _1GenericClass {

    public static void main(String[] args) {
        PersonPair person = new PersonPair(17, "Foo");
        //PersonPair person2 = new PersonPair(17, 20);// COMPILATION ERROR: 20 is not an String
        if (person.left < 18) {
            System.out.printf("Person: %s is not adult \n", person.right);
        }

        RangePair rangePair = new RangePair(Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.printf("Integer must be between %d and %d \n", rangePair.left, rangePair.right);
    }
}

class Pair<T, S> {

    T left;
    S right;

    Pair(T left, S right) {
        this.left = left;
        this.right = right;
    }

}

class PersonPair extends Pair<Integer, String> {

    public PersonPair(Integer left, String right) {
        super(left, right);
    }

}

class RangePair extends Pair<Integer, Integer> {

    public RangePair(Integer left, Integer right) {
        super(left, right);
    }

}
