/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.generics;

/**
 * Example for simple generic implementation.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _2GenericInherit {

    public static void main(String[] args) {
        Mamal cat = new Mamal("Miau");
        Mamal cow = new Mamal("Mooo");
        Oviparous hen = new Oviparous("cookoo");

        //HelloAnimal<String> helloStrings = new HelloAnimal<>(); //COMPILATION ERROR: becouse String is not an Animal
        HelloAnimal<Mamal> helloMamals = new HelloAnimal<>();
        System.out.println(helloMamals.sayHello(cat));
        System.out.println(helloMamals.sayHello(cow));
        // System.out.println(helloMamals.sayHello(hen)); //COMPILATION ERROR: becouse hen is not an Mamal

        HelloAnimal<Animal> helloAnimals = new HelloAnimal<>();
        System.out.println(helloAnimals.sayHello(hen)); // hen is an Animal too

    }
}

class HelloAnimal<T extends Animal> {

    String sayHello(T animal) {
        return animal.hello;
    }
}

class Animal {

    String hello;

    public Animal(String name) {
        this.hello = name;
    }

}

class Mamal extends Animal {

    public Mamal(String name) {
        super(name);
    }

}

class Oviparous extends Animal {

    public Oviparous(String name) {
        super(name);
    }

}
