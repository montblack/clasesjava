/**
 * Author:  <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 * Created: 28/03/2017
 */
CREATE TABLE Power (
    power_name VARCHAR(100) NOT NULL,
    power_value INTEGER NOT NULL,
    PRIMARY KEY (power_name)
);

CREATE TABLE Hero (
    hero_name VARCHAR(100) NOT NULL,
    race VARCHAR(20) NOT NULL,
    PRIMARY KEY (hero_name)
);

CREATE TABLE HeroPower (
    hero_name VARCHAR(100) NOT NULL,
    power_name VARCHAR(100) NOT NULL,
    CONSTRAINT hero_name_fk FOREIGN KEY (hero_name) REFERENCES Hero(hero_name),
    CONSTRAINT power_name_fk FOREIGN KEY (power_name) REFERENCES Power(power_name),
    PRIMARY KEY (hero_name, power_name)
);