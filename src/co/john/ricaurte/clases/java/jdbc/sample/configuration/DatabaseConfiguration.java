/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.configuration;

import co.john.ricaurte.clases.java.jdbc.sample.utils.FileUtils;
import co.john.ricaurte.clases.java.jdbc.sample.utils.QueryUtil;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class DatabaseConfiguration {

    public static void configureDatabase() {
        if (tablesExists()) {
            excecuteFile("dropTables.sql");
        }
        excecuteFile("createTables.sql");
    }

    private static boolean tablesExists() {
        Integer count = QueryUtil.queryCount("SELECT COUNT(1) FROM sys.systables "
                + "WHERE TABLENAME IN ('HERO', 'POWER', 'HEROPOWER')");
        return count != null && count > 0;
    }

    private static void excecuteFile(String fileName) {
        try {
            String[] instructions = FileUtils.getFileContentAsString(fileName).split(";");
            for (String instruction : instructions) {
                if (!instruction.trim().isEmpty()) {
                    QueryUtil.update(instruction);
                }
            }
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(DatabaseConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
