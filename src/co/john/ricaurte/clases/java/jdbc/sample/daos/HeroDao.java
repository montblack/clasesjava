/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.daos;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.HeroJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ParameterJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.PowerJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ResultSetMapper;
import co.john.ricaurte.clases.java.jdbc.sample.utils.QueryUtil;
import java.util.List;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class HeroDao extends AbstractDao<Hero> {

    HeroJdbcMapper mapper = new HeroJdbcMapper();

    @Override
    protected ResultSetMapper<Hero> getDefaultResultsetMapper() {
        return mapper;
    }

    @Override
    protected String getInsertSql() {
        return "INSERT INTO Hero (hero_name, race) VALUES (?, ?)";
    }

    @Override
    protected String getTableName() {
        return "HERO";
    }

    @Override
    protected String getPrimaryKeyColumnName() {
        return "HERO_NAME";
    }

    @Override
    protected ParameterJdbcMapper<Hero> getDefaultParameterMapper() {
        return mapper;
    }

    public void addPower(Hero hero, Power power) {
        QueryUtil.update("INSERT INTO HeroPower (hero_name, power_name) VALUES (?, ?)",
                hero.getName(), power.getName());
    }

    public void removePower(Hero hero, Power power) {
        QueryUtil.update("DELETE FROM HeroPower WHERE hero_name = ? AND power_name = ?",
                hero.getName(), power.getName());
    }

    public List<Power> findHeroPowers(Hero hero) {
        return QueryUtil.query("SELECT p.* FROM Power p "
                + "INNER JOIN HeroPower hp ON p.power_name = hp.power_name "
                + "WHERE hp.hero_name = ? ", new PowerJdbcMapper(), hero.getName());
    }
}
