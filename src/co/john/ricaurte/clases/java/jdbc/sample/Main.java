/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample;

import co.john.ricaurte.clases.java.jdbc.sample.configuration.DatabaseConfiguration;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DefaultData;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Race;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Main {

    public static final String LINE_SPLIT = "---------------------------------------";
    HeroService service = new HeroService();
    Scanner sc = new Scanner(System.in);

    void init() {
        performInitialDataAndConfiguration();
        Hero hero = createHero(createPower());
        addNewPower(hero);
        showAll();
    }

    private void showAll() {
        System.out.println("\n\nAll hero with powers list:\n\n");
        List<Hero> heros = service.findAllHeros();
        for (Hero hero : heros) {
            System.out.println(hero);
            System.out.println(LINE_SPLIT);
            hero.setPowers(service.getHeroPowers(hero));
            for (Power power: hero.getPowers()) {
                System.out.println(power);
            }
            System.out.println(LINE_SPLIT);
        }
    }

    private void addNewPower(Hero hero) {
        System.out.println(LINE_SPLIT);
        List<Power> powers = service.findAllPowers();
        System.out.println("\nNow add other power");
        System.out.println("current powers:");
        for (Power power : powers) {
            System.out.println(power);
        }
        System.out.println("Type the power name:");
        service.addNewPower(hero, sc.nextLine());
        System.out.println("Power added");
    }

    private Power createPower() throws NumberFormatException {
        System.out.println("Create a new Power.\nSet power name:");
        String name = sc.nextLine();
        System.out.println("Set power value:");
        Power power = new Power(name, Integer.valueOf(sc.nextLine()));
        service.createPower(power);
        return power;
    }

    private Hero createHero(Power power) {
        System.out.println(LINE_SPLIT);
        System.out.println("Create a new Hero.\nSet hero name:");
        String name = sc.nextLine();
        System.out.println("Set race:");
        Hero hero = new Hero(name, Race.valueOf(sc.nextLine()), power);
        service.createNewHero(hero);
        System.out.printf("Hero created '%s' with power '%s'", hero, power);
        return hero;
    }

    private void performInitialDataAndConfiguration() {
        DatabaseConfiguration.configureDatabase();
        DefaultData defaultData = new DefaultData();
        List<Power> powers = defaultData.createInitialPowers();
        List<Hero> heros = defaultData.createInitialHeros(powers);

        service.createAllPowers(powers);
        service.createAllHeros(heros);
    }

    public static void main(String[] args) {
        new Main().init();
    }
}
