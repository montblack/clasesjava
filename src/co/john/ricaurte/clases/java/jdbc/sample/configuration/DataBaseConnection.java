/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class DataBaseConnection {

    private static final String INMEMORY_CONNECTION_URL = "jdbc:derby:memory:testDB";
    private static final String CONNECTION_URL = "jdbc:derby:testDB";
    private static final Properties PROPERTIES;
    private static Connection connection;
    static {
        PROPERTIES = System.getProperties();
        PROPERTIES.setProperty("derby.system.home", System.getProperty("user.home"));
        PROPERTIES.setProperty("create", "true");
    }

    public static Connection getInMemoryConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(INMEMORY_CONNECTION_URL, PROPERTIES);
        }
        return connection;
    }

    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection(CONNECTION_URL, PROPERTIES);
        }
        return connection;
    }

}
