/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.daos;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ParameterJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.PowerJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ResultSetMapper;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class PowerDao extends AbstractDao<Power> {

    PowerJdbcMapper mapper = new PowerJdbcMapper();

    @Override
    protected ResultSetMapper<Power> getDefaultResultsetMapper() {
        return mapper;
    }

    @Override
    protected String getInsertSql() {
        return "INSERT INTO Power(power_name, power_value) VALUES (?,?)";
    }

    @Override
    protected String getTableName() {
        return "Power";
    }

    @Override
    protected String getPrimaryKeyColumnName() {
        return "power_name";
    }

    @Override
    protected ParameterJdbcMapper<Power> getDefaultParameterMapper() {
        return mapper;
    }
}
