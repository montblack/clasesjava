/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.daos;

import co.john.ricaurte.clases.java.jdbc.sample.mappers.ParameterJdbcMapper;
import co.john.ricaurte.clases.java.jdbc.sample.utils.QueryUtil;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ResultSetMapper;
import java.util.List;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 * @param <T>
 */
public abstract class AbstractDao<T> {

    public void insert(T object) {
        QueryUtil.executeUpdateWithMapper(getInsertSql(),
                getDefaultParameterMapper(), object);
    }

    public T findOne(Object primaryKey) {
        List<T> result = QueryUtil.query("SELECT * FROM " + getTableName()
                + " WHERE " + getPrimaryKeyColumnName() + " = ?",
                getDefaultResultsetMapper(), primaryKey);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<T> findAll() {
        return QueryUtil.query("SELECT * FROM " + getTableName(),
                getDefaultResultsetMapper());
    }

    protected abstract ResultSetMapper<T> getDefaultResultsetMapper();

    protected abstract ParameterJdbcMapper<T> getDefaultParameterMapper();

    protected abstract String getInsertSql();

    protected abstract String getTableName();

    protected abstract String getPrimaryKeyColumnName();

}
