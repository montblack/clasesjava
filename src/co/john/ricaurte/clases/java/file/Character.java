/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.file;

import java.io.Serializable;

/**
 * Character entity class.
 *
 * must implements Serializable for data serialization.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Character implements Serializable {

    /**
     * Character name.
     */
    private String name;

    /**
     * Character last name. Can be null.
     */
    private String lastName;

    /**
     * Character {@link Race}.
     */
    private Race race;

    /**
     * Character constructor with initial parameters
     *
     * @param name character name.
     * @param lastName character last name. can be null.
     * @param race {@link Race} character race.
     */
    public Character(String name, String lastName, Race race) {
        this.name = name;
        this.lastName = lastName;
        this.race = race;
    }

    /**
     * Get the value of race
     *
     * @return the value of race
     */
    public Race getRace() {
        return race;
    }

    /**
     * Set the value of race
     *
     * @param race new value of race
     */
    public void setRace(Race race) {
        this.race = race;
    }

    /**
     * Get the value of lastName
     *
     * @return the value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the value of lastName
     *
     * @param lastName new value of lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", lastName=" + lastName + ", race=" + race + '}';
    }

}
