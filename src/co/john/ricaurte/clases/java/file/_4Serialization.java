/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.file;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * IO file manage using object's serialization.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _4Serialization {

    /**
     * File url path
     */
    static final String FILE_PATH = System.getProperty("user.home") + "/testfile.ser";

    /**
     * Reads the file content, with NIO.
     *
     * @return List of {@link Character} readed.
     */
    List<Character> readFileContent() {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(Files.readAllBytes(Paths.get(FILE_PATH)));
                ObjectInput in = new ObjectInputStream(bis)) {
            return (List<Character>) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(_4Serialization.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Writes the character list on a serialized byte file.
     *
     * @param characterList list of {@link Character} to write.
     */
    private void writeCharactersToFile(List<Character> characterList) {
        try (ObjectOutput out = new ObjectOutputStream(new FileOutputStream(FILE_PATH))) {
            out.writeObject(characterList);
        } catch (IOException ex) {
            Logger.getLogger(_4Serialization.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Program start here.
     */
    public void init() {
        // Write
        List<Character> createCharacters = CharacterFactory.createCharacters();
        writeCharactersToFile(createCharacters);
        // Read
        List<Character> personList = readFileContent();
        // Print
        for (Character person : personList) {
            System.out.println(person);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new _4Serialization().init();
    }

}
