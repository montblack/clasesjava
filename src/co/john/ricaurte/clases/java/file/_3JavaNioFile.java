/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.file;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * IO file manage using java7's NIO.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _3JavaNioFile {

    /**
     * File url path
     */
    static final String FILE_PATH = System.getProperty("user.home") + "/testfile.txt";

    /**
     * Reads the file content, with NIO.
     *
     * @return List with line content for each row.
     */
    List<String> readFileContent() {
        try {
            return Files.readAllLines(Paths.get(FILE_PATH));
        } catch (IOException ex) {
            Logger.getLogger(_3JavaNioFile.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Writes the character list on a file, with NIO.
     *
     * @param characterList list of {@link Character} to write.
     */
    private void writeCharactersToFile(List<Character> characterList) {

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(FILE_PATH))) {
            for (Character character : characterList) {
                writer.append(character.getName()).append(System.lineSeparator())
                        .append(character.getLastName()).append(System.lineSeparator())
                        .append(character.getRace().name()).append(System.lineSeparator());
            }
        } catch (IOException ex) {
            Logger.getLogger(_3JavaNioFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Converts the file content into a character list.
     *
     * @param content List with line content for each row.
     * @return the converted list of {@link Character}.
     */
    List<Character> getAsCharacterList(List<String> content) {
        List<Character> personList = new ArrayList<>(content.size() / 3);
        for (int i = 0; i < content.size(); i += 3) {
            personList.add(new Character(
                    content.get(i),
                    content.get(i + 1),
                    Race.valueOf(content.get(i + 2))
            ));
        }
        return personList;
    }

    /**
     * Program start here.
     */
    public void init() {
        // Write
        List<Character> createCharacters = CharacterFactory.createCharacters();
        writeCharactersToFile(createCharacters);
        // Read
        List<String> content = readFileContent();
        List<Character> personList = getAsCharacterList(content);
        // Print
        for (Character person : personList) {
            System.out.println(person);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new _3JavaNioFile().init();
    }

}
