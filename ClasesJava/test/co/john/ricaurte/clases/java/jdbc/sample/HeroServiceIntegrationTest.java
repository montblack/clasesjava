/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Race;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DatabaseConfiguration;
import co.john.ricaurte.clases.java.jdbc.sample.configuration.DefaultData;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import java.util.List;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class HeroServiceIntegrationTest {

    static HeroService service = new HeroService();

    @BeforeClass
    public static void setUpClass() {
        DatabaseConfiguration.configureDatabase();
        DefaultData defaultData = new DefaultData();
        List<Power> powers = defaultData.createInitialPowers();
        List<Hero> heros = defaultData.createInitialHeros(powers);
        service.createAllPowers(powers);
        service.createAllHeros(heros);
    }

    @Test
    public void createHero_ShouldCreateHeroAndAddPowers() {
        Hero hero = new Hero("Testman", Race.MUTANT,
                new Power("TestPower", 20),
                new Power("TestSuperPower", 30));

        service.createNewHero(hero);

        Hero foundHero = service.findByHeroName("Testman");
        MatcherAssert.assertThat(foundHero, IsEqual.equalTo(hero));
    }
}
