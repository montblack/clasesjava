/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.generics;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _3GenericMethod {

    public static void main(String[] args) {
        Object objectValue = 100;
        Integer integerValue;

        integerValue = getAsTypeInfers(objectValue);
        integerValue = getAsTypeSpecified(Integer.class, objectValue);
        //integerValue = getAsTypeSpecified(String.class, test); // COMPILATION ERROR: integerValue is not a String type.
        integerValue = getAsTypeInfers("1000"); // RUNTIME EXCEPTION: "1000" is a String not a Integer
    }

    static <T> T getAsTypeInfers(Object value) {
        return (T) value;
    }

    static <T> T getAsTypeSpecified(Class<T> clazz, Object value) {
        return clazz.cast(value);
    }
}
