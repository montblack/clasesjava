/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.generics;

/**
 * Example for simple generic implementation.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _4GenericInterface {

    public static void main(String[] args) {
        String[] persons = {"Foo 20", "Bar 40", "Baz 18"};
        String[] numbers = {"100", "200", "300"};
        for (int i = 0; i < 3; i++) {
            System.out.print(formateedString(new IntegerConverter(), numbers[i]));
            System.out.print("\t");
            System.out.println(formateedString(new PersonConverter(), persons[i]));

        }

    }

    static String formateedString(Converter converter, String line) {
        return converter.convert(line).toString();
    }
}

/**
 * Generic interface for converters.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 * @param <T> generic type to be converted.
 */
interface Converter<T> {

    /**
     * Method who converts a String into Generic type.
     *
     * @param content String source
     * @return converted value of T type.
     */
    T convert(String content);
}

/**
 * {@link Converter} implementation with Integer type.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
class IntegerConverter implements Converter<Integer> {

    @Override
    public Integer convert(String content) {
        return Integer.valueOf(content) / 100;
    }

}

/**
 * Person POJO.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
class Person {

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }

}

/**
 * {@link Converter} implementation with {@link Person} type.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
class PersonConverter implements Converter<Person> {

    @Override
    public Person convert(String content) {
        String[] parts = content.split(" ");
        return new Person(parts[0], Integer.parseInt(parts[1]));
    }

}
