/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Character entity class.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Hero {

    /**
     * Character name.
     */
    private String name;

    /**
     * Character {@link Race}.
     */
    private Race race;

    /**
     * Hero powers.
     */
    private List<Power> powers = new ArrayList<>();

    /**
     * Character constructor with initial parameters
     *
     * @param name character name.
     * @param race {@link Race} character race.
     * @param powers list of {@link Power} attatched to the current hero.
     */
    public Hero(String name, Race race, Power... powers) {
        this.name = name;
        this.race = race;
        this.powers.addAll(Arrays.asList(powers));
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of race
     *
     * @return the value of race
     */
    public Race getRace() {
        return race;
    }

    /**
     * Set the value of race
     *
     * @param race new value of race
     */
    public void setRace(Race race) {
        this.race = race;
    }

    /**
     * Get the value of powers
     *
     * @return the value of powers
     */
    public List<Power> getPowers() {
        return powers;
    }

    /**
     * Set the value of powers
     *
     * @param powers new value of powers
     */
    public void setPowers(List<Power> powers) {
        this.powers = powers;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hero other = (Hero) obj;
        return Objects.equals(this.name, other.name);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", race=" + race + '}';
    }

}
