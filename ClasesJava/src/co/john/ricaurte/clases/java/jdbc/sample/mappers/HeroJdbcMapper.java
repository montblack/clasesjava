/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.mappers;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Race;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class HeroJdbcMapper implements ResultSetMapper<Hero>, ParameterJdbcMapper<Hero> {

    @Override
    public Hero convert(ResultSet resultSet) throws SQLException {
        return new Hero(resultSet.getString("hero_name"), 
                Race.valueOf(resultSet.getString("race")));
    }

    @Override
    public void mapValues(PreparedStatement statement, Hero hero) throws SQLException {
        if (hero.getName() != null) {
            statement.setString(1, hero.getName());
        }
        if (hero.getRace() != null) {
            statement.setString(2, hero.getRace().name());
        }
    }

}
