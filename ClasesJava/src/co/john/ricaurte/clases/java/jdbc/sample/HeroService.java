/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import co.john.ricaurte.clases.java.jdbc.sample.daos.PowerDao;
import co.john.ricaurte.clases.java.jdbc.sample.daos.HeroDao;
import java.util.List;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class HeroService {

    private final HeroDao heroDao = new HeroDao();
    private final PowerDao powerDao = new PowerDao();

    public void createNewHero(Hero hero) {
        heroDao.insert(hero);
        for (Power power : hero.getPowers()) {
            heroDao.addPower(hero, power);
        }
    }

    public void addNewPower(Hero hero, String powerName) {
        Power power = powerDao.findOne(powerName);
        if (power != null) {
            hero.setPowers(heroDao.findHeroPowers(hero));
            if (!hero.getPowers().contains(power)) {
                hero.getPowers().add(power);
                heroDao.addPower(hero, power);
            }
        }
    }

    public Hero findByHeroName(String heroName) {
        Hero hero = heroDao.findOne(heroName);
        hero.setPowers(heroDao.findHeroPowers(hero));
        return hero;
    }

    public void createAllPowers(List<Power> powers) {
        for (Power power : powers) {
            powerDao.insert(power);
        }
    }

    public void createAllHeros(List<Hero> heros) {
        for (Hero hero : heros) {
            createNewHero(hero);
        }
    }

    public List<Power> findAllPowers() {
        return powerDao.findAll();
    }

    void createPower(Power power) {
        powerDao.insert(power);
    }

    List<Hero> findAllHeros() {
        return heroDao.findAll();
    }
    
    List<Power> getHeroPowers(Hero hero){
        return heroDao.findHeroPowers(hero);
    }
}
