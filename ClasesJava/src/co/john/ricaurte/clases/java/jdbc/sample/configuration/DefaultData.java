/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.configuration;

import co.john.ricaurte.clases.java.jdbc.sample.entities.Race;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Hero;
import co.john.ricaurte.clases.java.jdbc.sample.entities.Power;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class DefaultData {

    private static final String FLY_POWER = "Fly";
    private static final String XRAY_POWER = "Xray vision";
    private static final String VELOCITY_POWER = "Super velocity";
    private static final String FORCE_POWER = "Super force";
    private static final String TEC_POWER = "Tecnology stuff";
    private static final String SUPERMAN_HERO = "Superman";
    private static final String FLASH_HERO = "Flash";
    private static final String BATMAN_HERO = "Batman";

    public List<Power> createInitialPowers() {
        return Arrays.asList(
                new Power(FLY_POWER, 10),
                new Power(XRAY_POWER, 20),
                new Power(VELOCITY_POWER, 30),
                new Power(FORCE_POWER, 30),
                new Power(TEC_POWER, 20)
        );
    }

    public List<Hero> createInitialHeros(List<Power> initialPowers) {
        return Arrays.asList(
                new Hero(SUPERMAN_HERO, Race.ALIEN, 
                        initialPowers.get(0), initialPowers.get(1),
                        initialPowers.get(2), initialPowers.get(3)),
                new Hero(FLASH_HERO, Race.HUMAN, initialPowers.get(2)),
                new Hero(BATMAN_HERO, Race.HUMAN, initialPowers.get(4))
        );
    }
}
