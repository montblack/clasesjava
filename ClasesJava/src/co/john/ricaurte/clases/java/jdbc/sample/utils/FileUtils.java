/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.utils;

import co.john.ricaurte.clases.java.jdbc.sample.configuration.DatabaseConfiguration;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class FileUtils {

    public static String getFileContentAsString(String filename) throws URISyntaxException, IOException {
        URI resourceUri = DatabaseConfiguration.class.getResource(filename).toURI();
        return new String(Files.readAllBytes(Paths.get(resourceUri)));
    }
}
