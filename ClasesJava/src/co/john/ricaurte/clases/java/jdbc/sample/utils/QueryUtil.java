/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc.sample.utils;

import co.john.ricaurte.clases.java.jdbc.sample.configuration.DataBaseConnection;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ParameterJdbcMapper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.john.ricaurte.clases.java.jdbc.sample.mappers.ResultSetMapper;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class QueryUtil {

    public static Integer queryCount(String sql, Object... params) {
        try (PreparedStatement statement = DataBaseConnection.getConnection()
                .prepareStatement(sql)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            ResultSet executeQuery = statement.executeQuery();
            executeQuery.next();
            return executeQuery.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(QueryUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static <T> List<T> query(String sql, ResultSetMapper<T> mapper, Object... params) {
        try (PreparedStatement statement = DataBaseConnection.getConnection()
                .prepareStatement(sql)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
           
            ResultSet resultSet = statement.executeQuery();
            List<T> results = new ArrayList<>();
            while(resultSet.next()){
                results.add(mapper.convert(resultSet));
            }
            return results;
        } catch (SQLException ex) {
            Logger.getLogger(QueryUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static <T> int executeUpdateWithMapper(String sql, ParameterJdbcMapper<T> mapper, T object ) {
        try (PreparedStatement statement = DataBaseConnection.getConnection()
                .prepareStatement(sql)) {
            mapper.mapValues(statement, object);
            return statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QueryUtil.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public static int update(String sql, Object... params) {
        try (PreparedStatement statement = DataBaseConnection.getConnection()
                .prepareStatement(sql)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QueryUtil.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
}
