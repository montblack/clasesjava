/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *aaaa
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class SimpleExample {

    private final List<Person> people = Arrays.asList(
            new Person(null, 20, "FEMALE", "Foo"),
            new Person(null, 17, "MALE", "Bar"),
            new Person(null, 18, "MALE", "Baz"),
            new Person(null, 30, "FEMALE", "Qux")
    );

    void init() {
        createDdl();
        insertPeople();
        List<Person> adultPeople = findAdultPeople(); 
        for(Person person: adultPeople){
            System.out.println(person);
        }
    }

    /**
     * Find the database people who is adult (age >= 18).
     * <p>
     * Uses a resultset to iterate over results, the resultset just go forward it can't go back with
     * a allready read result.
     *
     * @return
     */
    List<Person> findAdultPeople() {
        try (Connection connection = getConnection();
                PreparedStatement prepareStatement = connection
                        .prepareStatement("SELECT p.* FROM Person p WHERE p.age >= ? ");) {

            // Set parameters
            prepareStatement.setInt(1, 18);

            // Get the results and iterate
            ResultSet resultSet = prepareStatement.executeQuery();
            List<Person> foundPersons = new ArrayList<>();
            while (resultSet.next()) {

                // adds the result to the found person list
                foundPersons.add(new Person(
                        resultSet.getInt("id"),
                        resultSet.getInt("age"),
                        resultSet.getString("gender"),
                        resultSet.getString("name")));
            }
            return foundPersons;
        } catch (SQLException ex) {
            Logger.getLogger(SimpleExample.class.getName()).log(Level.SEVERE, null, ex);
            return Collections.emptyList();
        }
    }

    /**
     * Inserts the {@link #people} in database.
     * <p>
     * Uses a {@link PreparedStatement} to create a statement with the benefits of use '?' wilcard
     * for parameters and avoid the SQL injection.
     * <p>
     * At the end gets the database generated IDs by using the hint
     * {@link Statement#RETURN_GENERATED_KEYS}.
     */
    void insertPeople() {
        String InsertSql = "INSERT INTO Person (age, gender, name) "
                + "VALUES (?, ?, ?)";
        try (Connection connection = getConnection();
                PreparedStatement prepareStatement = connection
                        .prepareStatement(InsertSql, Statement.RETURN_GENERATED_KEYS);) {

            for (Person person : people) {
                // Set values to wilcards ?
                prepareStatement.setInt(1, person.age);
                prepareStatement.setString(2, person.gender);
                prepareStatement.setString(3, person.name);

                // excecute the query
                prepareStatement.executeUpdate();

                // get generated keys
                try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        person.id = generatedKeys.getInt(1);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(SimpleExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets the connection from {@link #getConnection()} and creates a new table "Person".
     */
    void createDdl() {
        try (Connection connection = getConnection();
                Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE Person (   "
                    + "    id       INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                    + "    age      INTEGER NOT NULL, "
                    + "    gender   VARCHAR(6), "
                    + "    name     VARCHAR(100), "
                    + "    PRIMARY KEY (id) "
                    + ")");
        } catch (SQLException ex) {
            Logger.getLogger(SimpleExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Obtains the database connection from a in-memory derby database.
     * <p>
     * Remember the application should has the properly JDBC driver from the database, every time
     * you call getConnection is creating a new phisical connection with the database you can check
     * the current connection status with the following code {@code connection.isClosed()}.
     * <p>
     * In this case the database is in-memory so every time you start the program the database is
     * created from zero, for other connection option for derby database see the documentation:
     * <a href="https://db.apache.org/derby/docs/10.4/devguide/cdevdvlp17453.html"> derby connection
     * string (version 10.4)</a>
     *
     * @return the new {@link Connection} object.
     * @throws SQLException if the driver is not configure or the database has a problem.
     */
    Connection getConnection() throws SQLException {
        Properties properties = System.getProperties();
        properties.setProperty("derby.system.home", System.getProperty("user.home"));
        properties.setProperty("create", "true");
        properties.setProperty("user", "testUser");
        properties.setProperty("password", "testPassword");

        return DriverManager.getConnection("jdbc:derby:memory:testDB", properties);
    }

    public static void main(String[] args) {
        new SimpleExample().init();
    }

}

class Person {

    Integer id;
    Integer age;
    String gender;
    String name;

    public Person(Integer id, Integer age, String gender, String name) {
        this.id = id;
        this.age = age;
        this.gender = gender;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", age=" + age + ", gender=" + gender + ", name=" + name + '}';
    }

}
