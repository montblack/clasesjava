/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.inherit;

/**
 * Represent the Boss data extends from {@link Employee}
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Boss extends Employee {

    /**
     * Creates a new Boss with de mandatory values.
     *
     * @param name boss name.
     * @param salary boss salary.
     */
    Boss(String name, double salary) {
        super(name);
        this.salary = salary;
    }

    /**
     * Order the boss to coordinate.
     */
    void coordinate() {
        System.out.println("Boss " + name + " is now coordinating");
    }

    /**
     * {@inheritDoc }. The boss adds a bonus
     */
    @Override
    void paySalary() {
        System.out.println("Pay " + salary + " plus " + 50 + " bonus to " + name);
    }
}
