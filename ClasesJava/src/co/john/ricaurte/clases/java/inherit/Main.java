/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.inherit;

/**
 * Main example class.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Main {

    /**
     * Creeates a list of {@link Employee} and if is a {@link Boss} then calls
     * {@link Boss#coordinate()}. then prints the salary of each employee,
     * notice that Boss override the method who print the salary.
     *
     * @param args
     */
    public static void main(String[] args) {

        Employee[] employees = {new Worker("Donatello"), new Boss("Splinter", 1200)};

        for (Employee emp : employees) {

            if (emp instanceof Boss) { // checks if is a Boss to avoid cast error

                Boss boss = (Boss) emp; // cast the employee to a boss
                boss.coordinate(); // call coordinate if is a boss
            }
            emp.paySalary();// call salary
        }
    }
}
