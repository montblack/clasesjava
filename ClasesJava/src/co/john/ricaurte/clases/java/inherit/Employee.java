/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.inherit;

/**
 * Represents the Employee data
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class Employee {

    /**
     * Employee name
     */
    String name;
    /**
     * Employee salary
     */
    double salary;

    /**
     * Creates a new Employee with the mandatory data.
     *
     * @param name Employee name.
     */
    Employee(String name) {
        this.name = name;
    }

    /**
     * Prints the salary and the name of the employee.
     */
    void paySalary() {
        System.out.println("Pay " + salary + " to " + name);
    }
}
