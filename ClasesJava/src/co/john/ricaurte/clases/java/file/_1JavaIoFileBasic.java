/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic IO file manage.
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _1JavaIoFileBasic {

    /**
     * File url path
     */
    static final String FILE_PATH = System.getProperty("user.home") + "/testfile.txt";

    /**
     * Reads the file content.
     *
     * @return content String.
     */
    String readFileContent() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(FILE_PATH));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();
        } catch (IOException ex) {
            Logger.getLogger(_1JavaIoFileBasic.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(_1JavaIoFileBasic.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    /**
     * Writes the character list on a file.
     *
     * @param characterList list of {@link Character} to write.
     */
    private void writeCharactersToFile(List<Character> characterList) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(FILE_PATH);
            for (Character character : characterList) {
                writer.println(character.getName());
                writer.println(character.getLastName());
                writer.println(character.getRace());
            }
        } catch (IOException ex) {
            Logger.getLogger(_1JavaIoFileBasic.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * Converts the file content into a character list.
     *
     * @param content file content String
     * @return the converted list of {@link Character}.
     */
    List<Character> getAsCharacterList(String content) {
        String[] lines = content.split(System.lineSeparator());
        List<Character> characterList = new ArrayList<>(lines.length / 3);
        for (int i = 0; i < lines.length; i += 3) {
            characterList.add(new Character(
                    lines[i],
                    lines[i + 1],
                    Race.valueOf(lines[i + 2])
            ));
        }
        return characterList;
    }

    /**
     * Program start here.
     */
    public void init() {
        // Write
        List<Character> createCharacters = CharacterFactory.createCharacters();
        writeCharactersToFile(createCharacters);
        // Read
        String content = readFileContent();
        List<Character> personList = getAsCharacterList(content);
        // Print
        for (Character person : personList) {
            System.out.println(person);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new _1JavaIoFileBasic().init();
    }

}
