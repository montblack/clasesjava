/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.file;

import java.util.Arrays;
import java.util.List;

/**
 * Factory of characters ({@link Character}).
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class CharacterFactory {

    /**
     * Creates a new list of TMNT series characters.
     *
     * @return the list of created {@link Character}.
     */
    public static List<Character> createCharacters() {
        return Arrays.asList(
                new Character("Donatello", null, Race.TORTOISE),
                new Character("Splinter", null, Race.RAT),
                new Character("April", "O'Neil", Race.HUMAN),
                new Character("Casey", "Jones", Race.HUMAN),
                new Character("Bebop", null, Race.WILD_BOARD),
                new Character("Rocksteady ", null, Race.RHINO),
                new Character("Leonardo", null, Race.TORTOISE),
                new Character("Raphael", null, Race.TORTOISE),
                new Character("Oroku", "Saki", Race.HUMAN),
                new Character("Michelangelo", null, Race.TORTOISE));
    }
}
