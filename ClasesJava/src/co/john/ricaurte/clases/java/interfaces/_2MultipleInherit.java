/*
 * Copyright (C) 2017 <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package co.john.ricaurte.clases.java.interfaces;

/**
 *
 * @author <a href="mailto:montblackjj@gmail.com">John Ricaurte</a>
 */
public class _2MultipleInherit {

    public static void main(String[] args) {
        Amphibious amphibious = new Amphibious();

        terranRun(amphibious);
        waterNavigate(amphibious);
    }

    static void terranRun(TeranVehicle teranVehicle) {
        teranVehicle.run();
    }

    static void waterNavigate(WaterVehicle waterVehicle) {
        waterVehicle.navigate();
    }
}

class TeranVehicle {

    void run() {
        System.out.println("Running...");
    }
}

interface WaterVehicle {

    void navigate();
}

class Amphibious extends TeranVehicle implements WaterVehicle {

    @Override
    public void navigate() {
        System.out.println("Navigating...");
    }

}
